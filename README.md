spring boot

<-> 开箱即用  

本项目是spring boot继承上级pom.xml，无需在意，运行common-service(spring boot项目)程序即可  

环境：
ide: `idea`  
插件`lombok`(必须), `Free Mybatis plugin`(推荐)  

配置：  
复制当前目录下`settings.xml`文件到用户目录: ${user.home}/.m2/settings.xml(如不存在，可直接复制进去)  

使用方式:  
1.新建数据库spring-boot,运行后jpa自动生成表  
2.com.wobangkj.common目录下api->crud目录:基本的jpa和mybatis的增删改查  
3.其他接口是一些通用接口, 可通过eolinker网站导入根目录下公共模块api.amsg文件查看(或者直接查看代码)  

ps: 通用的接口未来更新参考spring-cloud项目