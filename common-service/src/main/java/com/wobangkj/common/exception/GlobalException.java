package com.wobangkj.common.exception;

import com.wobangkj.handler.AbstractExceptionHandler;
import com.wobangkj.tool.api.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import static com.wobangkj.api.Response.ERR;
import static com.wobangkj.api.Response.fail;

/**
 * 全局异常处理
 *
 * @author cliod
 * @version 1.0
 * @since 2021-01-09 14:35:16
 */
@Slf4j
@RestControllerAdvice
public class GlobalException extends AbstractExceptionHandler {
	// 完整的判断中文汉字和符号
	public static boolean isChinese(String strName) {
		char[] ch = strName.toCharArray();
		for (char c : ch) {
			if (isChinese(c)) {
				return true;
			}
		}
		return false;
	}

	// 根据Unicode编码完美的判断中文汉字和符号
	private static boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION;
	}

	/**
	 * 异常处理
	 *
	 * @param e        异常
	 * @param request  请求
	 * @param response 响应
	 * @return 结果消息
	 */
	@Override
	@ExceptionHandler(value = Throwable.class)
	public Object handler(Throwable e, HttpServletRequest request, HttpServletResponse response) {
		log.error(e.getMessage());
		return ERR;
	}

	/**
	 * 其他运行时异常 + sql 运行是异常处理
	 *
	 * @param e 异常
	 * @return 结果消息
	 */
	@Override
	@ExceptionHandler(RuntimeException.class)
	public Object runtimeExceptionHandler(RuntimeException e) {
		log.error("未知错误：{}", e.getMessage());
		try {
			String msg = e.getCause().getMessage();
			if (msg.contains("Duplicate entry")) {
				String key = msg.substring(msg.indexOf("key '") + 5, msg.length() - 1);
				if (key.contains(".")) {
					key = key.split("\\.")[1];
				}
				return fail(key);
			}
		} catch (Exception e1) {
			return super.exceptionHandler(e);
		}
		return Result.GetMapData(Result.CodeError, Result.MsgError, e);
	}

	/**
	 * sql 异常
	 */
	@ExceptionHandler(value = SQLException.class)
	public Object sqlException(SQLException e) {
		log.error("未知错误：{}", e.getMessage(), e);
		return super.exceptionHandler(e);
	}
}
