package com.wobangkj.common.api.captcha;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.wobangkj.tool.api.result.MapData;
import com.wobangkj.tool.api.result.Result;
import com.wobangkj.tool.manager.cache.CacheManager;
import com.wobangkj.tool.manager.cache.impl.RedisManager;
import com.wobangkj.tool.model.CacheModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class Captcha {

	private static CacheManager cacheManager;

	// 验证
	public static MapData Check(String key, String code) {

		CacheModel cacheModel = cacheManager.get(key);
		if (cacheModel == null) {
			return Result.GetMapData(Result.CodeText, "验证码不存在或已失效");
		}
		// value equals value
		if (String.valueOf(cacheModel.getData()).equals(code)) {
			cacheManager.delete(key);
			return Result.MapSuccess;
		}
		return Result.MapValidateErr;
	}

	@Autowired
	public void setRedis(RedisConnectionFactory redisConnectionFactory) {
		cacheManager = new RedisManager(redisConnectionFactory);
	}

	/**
	 * 获取验证码
	 *
	 * @param response 响应
	 * @return 结果
	 */
	@GetMapping("/captcha")
	public Object captcha(@RequestParam(required = false) String key,
	                      HttpServletResponse response) {
		LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 50);
		cacheManager.set(key, new CacheModel(5L, lineCaptcha.getCode()));
		try {
			lineCaptcha.write(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}

	@GetMapping("/check")
	public Object CheckQReq(@RequestParam(required = false) String key, @RequestParam(required = false) String code) {
		return Check(key, code);
	}
}
