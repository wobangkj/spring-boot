package com.wobangkj.common.dao;

import com.wobangkj.api.IMapper;
import com.wobangkj.common.entity.Guide;

/**
 * 参观指南信息(Guide)表数据库访问层
 *
 * @author cliod
 * @since 2021-01-09 17:20:21
 */
public interface GuideDao extends IMapper<Guide> {

}