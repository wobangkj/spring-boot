package com.wobangkj.common.dao;

import com.wobangkj.common.model.crud.ShopDe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author dreamlu
 * @date 2019/04/01
 */
@Repository
@Mapper
public interface ShopDao {

	/**
	 * mybatis 原生sql
	 */
	List<ShopDe> searchDe(Map<String, Object> params);

	/**
	 * 更详细数据查询
	 *
	 * @param id
	 * @return
	 */
	List<ShopDe> IdDe(Long id);
}
