package com.wobangkj.common.entity;

import com.wobangkj.api.SessionSerializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 参观指南
 *
 * @author cliod
 * @version 1.0
 * @since 2021-01-09 17:13:33
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@org.hibernate.annotations.Table(appliesTo = "guide", comment = "参观指南信息")
public class Guide implements SessionSerializable {
	private static final long serialVersionUID = 6948916427412516579L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 创建时间
	 */
	@Column(columnDefinition = "datetime default current_timestamp() comment '创建时间'")
	private Date createTime;

	/**
	 * 所属商户小程序id
	 */
	@NotNull(message = "所属商户小程序id不能为空")
	@Column(columnDefinition = "bigint not null comment '所属商户小程序id'")
	private Long appletId;
	/**
	 * 富文本
	 */
	@Column(columnDefinition = "longtext comment '富文本'")
	private String content;
}
