package com.wobangkj.common.service;

import com.wobangkj.api.IService;
import com.wobangkj.common.entity.Guide;

/**
 * 参观指南信息(Guide)表服务接口
 *
 * @author cliod
 * @since 2021-01-09 17:20:21
 */
public interface GuideService extends IService<Guide> {

}