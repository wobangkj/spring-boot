package com.wobangkj.common.service;


import com.wobangkj.common.model.crud.Shop;
import com.wobangkj.common.model.crud.ShopDe;

import java.util.Map;

/**
 * 店铺信息
 */

public interface ShopService {

	Object search(Map<String, Object> params, Shop shop);

	Object id(Long id);

	Object update(Shop shop);

	Object create(Shop shop);

	Object delete(Long id);

	Object idDe(Long id);

	/**
	 * mybatis 原生sql查询
	 *
	 * @param params
	 * @param shop
	 * @return
	 */
	Object searchDe(Map<String, Object> params, ShopDe shop);

	/**
	 * jpa原生sql查询
	 */
	Object getShopByName(String name, Integer clientPage, Integer everyPage);
}
