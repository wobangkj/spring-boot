package com.wobangkj.common.service.impl;

import com.wobangkj.api.TkServiceImpl;
import com.wobangkj.common.dao.GuideDao;
import com.wobangkj.common.entity.Guide;
import com.wobangkj.common.service.GuideService;
import org.springframework.stereotype.Service;

/**
 * 参观指南信息(Guide)表服务实现类
 *
 * @author cliod
 * @since 2021-01-09 17:20:21
 */
@Service("guideService")
public class GuideServiceImpl extends TkServiceImpl<GuideDao, Guide> implements GuideService {
}