package com.wobangkj.common.util.wx;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonUtils extends com.wobangkj.utils.JsonUtils {
	private static final ObjectMapper JSON = new ObjectMapper();

	static {
		JSON.setSerializationInclusion(Include.NON_NULL);
		JSON.configure(SerializationFeature.INDENT_OUTPUT, Boolean.TRUE);
		setObjectMapper(JSON);
	}

}