package com.wobangkj.common.util.date;

import com.wobangkj.enums.Format;
import com.wobangkj.utils.DateUtils;

import java.time.LocalDate;

/**
 * @author dreamlu
 * @date 2019/06/22
 */
public class DateUtil {

	/**
	 * 获取几天前后日期
	 *
	 * @return
	 */
	public static String getTime(int day) {
		return DateUtils.format(LocalDate.now().plusDays(day), Format.DATE_DEFAULT);
	}
}
