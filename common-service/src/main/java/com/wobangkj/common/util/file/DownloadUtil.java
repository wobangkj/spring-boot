package com.wobangkj.common.util.file;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;

/**
 * @author dreamlu
 * @date 2019/06/26
 */

public class DownloadUtil {

	/**
	 * 下载文件---返回下载后的文件存储路径
	 *
	 * @param url      文件地址
	 * @param dir      存储目录
	 * @param fileName 存储文件名
	 * @return
	 */
	public static void downloadHttpUrl(String url, String dir, String fileName) {
		try {
			URL  httpurl = new URL(url);
			File dirfile = new File(dir);
			if (!dirfile.exists()) {
				dirfile.mkdirs();
			}
			FileUtils.copyURLToFile(httpurl, new File(dir + fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
