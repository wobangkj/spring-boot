package com.wobangkj.common.config.handler;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wobangkj.handler.BaseJsonTypeHandler;
import com.wobangkj.utils.JsonUtils;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class JsonTypeHandler<T extends Serializable> extends BaseJsonTypeHandler<T> {

	/**
	 * 返回映射的对象
	 *
	 * @param json 字符串
	 * @return 对象
	 */
	@Override
	protected T get(String json) {
		return this.toObject(json);
	}

	private T toObject(String content) {
		if (content == null || content.isEmpty()) {
			return null;
		}
		Class<?> clazz;
		if (content.startsWith("\"{")) {
			clazz = JSONObject.class;
		} else {
			clazz = JSONArray.class;
		}
		try {
			return (T) JsonUtils.fromJson(content, clazz);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}