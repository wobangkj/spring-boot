package com.wobangkj.common.config.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wobangkj.handler.BaseJsonTypeHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@Slf4j
@MappedTypes(JSONObject.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public class ObjectJsonHandler extends BaseJsonTypeHandler<JSONObject> {

	/**
	 * 返回映射的对象
	 *
	 * @param json 字符串
	 * @return 对象
	 */
	@Override
	protected JSONObject get(String json) {
		if (null != json) {
			return JSON.parseObject(json);
		}
		return null;
	}
}