package com.wobangkj.common.config;//package com.wobangkj.tool.config;
//
//import com.wobangkj.tool.config.url.FileInterceptor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//
//@Configuration
//public class WebConfigurer extends WebMvcConfigurationSupport {
//
//	@Bean
//	public FileInterceptor FileInterceptor() {
//		return new FileInterceptor();
//	}
//
//	/**
//	 * url 文件解析
//	 *
//	 * @param registry
//	 */
//	@Override
//	public void addInterceptors(InterceptorRegistry registry) {
//		// 拦截url
//		//registry.addInterceptor(FileInterceptor()).addPathPatterns("/static/file/**");
//		//registry.addInterceptor(AuthorityInterceptor()).addPathPatterns("/**");
//	}
//
//	/**
//	 * 静态资源拦截
//	 *
//	 * @param registry
//	 */
//	@Override
//	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/static/**").
//				addResourceLocations(
//						"classpath:resources/",
//						"file:common-service/static/");
//	}
//}