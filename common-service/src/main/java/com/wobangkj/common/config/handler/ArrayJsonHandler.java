package com.wobangkj.common.config.handler;

import com.alibaba.fastjson.JSONArray;
import com.wobangkj.handler.BaseJsonTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@MappedTypes(JSONArray.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public class ArrayJsonHandler extends BaseJsonTypeHandler<JSONArray> {
    /**
     * 返回映射的对象
     *
     * @param json 字符串
     * @return 对象
     */
    @Override
    protected JSONArray get(String json) {
        if (null != json){
            return JSONArray.parseArray(json);
        }
        return null;
    }
}