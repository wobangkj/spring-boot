package com.wobangkj.common.cron;

import com.wobangkj.common.util.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * 定时任务
 *
 * @author dreamlu
 * @date 2019/06/22
 */
@Component
@Slf4j
public class Cron {

	// 文件写入路径
	private static final String AppletLogDir = "/common-service/log/";

	// 文件名
	private static final String fileName = "front.log";

	// 今天
	private static String today = AppletLogDir + fileName;
	// 一天前
	private static String OLD   = AppletLogDir + fileName + "." + DateUtil.getTime(-1);
	// 两天前
	private static String OLD2  = AppletLogDir + fileName + "." + DateUtil.getTime(-2);
	// 三天前
	private static String OLD3  = AppletLogDir + fileName + "." + DateUtil.getTime(-3);
	// 四天前
	//private static final String OLD4 = AppletLogDir + DateUtil.getTime(-4) + "-" + fileName;

	// ================ 定时任务 ========================

	/**
	 * * 第一位，表示秒，取值0-59
	 * * 第二位，表示分，取值0-59
	 * * 第三位，表示小时，取值0-23
	 * * 第四位，日期天/日，取值1-31
	 * * 第五位，日期月份，取值1-12
	 * * 第六位，星期，取值1-7，星期一，星期二...，注：不是第1周，第二周的意思
	 * 另外：1表示星期天，2表示星期一。
	 * * 第7为，年份，可以留空，取值1970-2099
	 */
	@Scheduled(cron = "59 59 23 * * ?")
	public void frontFile() {
		log.error("[开始定时扫扫描更改front日志]");
		File rootFile = new File("");
		// 真实路径
		OLD3 = rootFile.getAbsolutePath() + OLD3;
		File src = new File(OLD3);
		// 删除三天前日志
		if (src.exists()) {
			src.delete();
		}
		// 两天前日志改名
		OLD2 = rootFile.getAbsolutePath() + OLD2;
		src = new File(OLD2);
		if (src.exists()) {
			src.renameTo(new File(OLD3));
		}

		// 一天前
		OLD = rootFile.getAbsolutePath() + OLD;
		src = new File(OLD);
		if (src.exists()) {
			src.renameTo(new File(OLD2));
		}

		// 今天
		today = rootFile.getAbsolutePath() + today;
		src = new File(today);
		if (src.exists()) {
			src.renameTo(new File(OLD));
		}
//		try {
//			new File(today).createNewFile();
//		} catch (IOException e) {
//			log.error("[今日front.log文件生成失败]");
//		}
	}
}
