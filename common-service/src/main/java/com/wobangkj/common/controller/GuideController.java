package com.wobangkj.common.controller;

import com.wobangkj.common.entity.Guide;
import com.wobangkj.common.service.GuideService;
import com.wobangkj.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

import static com.wobangkj.api.Response.DELETE;
import static com.wobangkj.api.Response.ok;

/**
 * 参观指南信息(Guide)表控制层
 *
 * @author cliod
 * @since 2021-01-09 17:23:10
 */
@RestController
@RequestMapping("/guide")
public class GuideController {
	/**
	 * 服务对象
	 */
	@Resource
	private GuideService guideService;

	/**
	 * 通过查询单条数据
	 *
	 * @param guide 参数
	 * @return 单条数据
	 */
	@GetMapping("/get")
	public Object get(Guide guide) {
		Object obj = this.guideService.queryOne(guide);
		org.springframework.util.Assert.notNull(obj, "暂无数据");
		return ok(obj);
	}

	/**
	 * 查询多条数据
	 *
	 * @param guide    参数
	 * @param pageable 分页、排序，模糊查询
	 * @return 列表
	 */
	@GetMapping("/search")
	public Object list(Guide guide, Pageable pageable) {
		return ok(this.guideService.queryAll(guide, pageable));
	}

	/**
	 * 添加一条数据
	 *
	 * @param guide 数据
	 * @return 结果
	 */
	@PostMapping("/create")
	public Object add(@Valid @RequestBody Guide guide) {
		return ok(this.guideService.insert(guide));
	}

	/**
	 * 编辑一条数据
	 *
	 * @param guide 数据
	 * @return 结果
	 */
	@PutMapping("/update")
	public Object edit(@RequestBody Guide guide) {
		return ok(this.guideService.update(guide));
	}

	/**
	 * 删除一条数据
	 *
	 * @param id id
	 * @return 结果
	 */
	@DeleteMapping("/delete/{id}")
	public Object del(@PathVariable Long id) {
		return this.guideService.deleteById(id) ? ok() : DELETE;
	}
}